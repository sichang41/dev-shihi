﻿namespace Issue58
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGVsubtitlelines = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGVsubtitles = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRead_subtitlelines = new System.Windows.Forms.Button();
            this.btnRead_subtitles = new System.Windows.Forms.Button();
            this.dataGVvideofiles = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRead_videofiles = new System.Windows.Forms.Button();
            this.btnInsert_videofiles = new System.Windows.Forms.Button();
            this.btnUpdate_videofiles = new System.Windows.Forms.Button();
            this.btnDelete_videofiles = new System.Windows.Forms.Button();
            this.btnClear_videofiles = new System.Windows.Forms.Button();
            this.btnRollback_videofiles = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGVsubtitlelines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGVsubtitles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGVvideofiles)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGVsubtitlelines
            // 
            this.dataGVsubtitlelines.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGVsubtitlelines.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.dataGVsubtitlelines.Location = new System.Drawing.Point(12, 47);
            this.dataGVsubtitlelines.Name = "dataGVsubtitlelines";
            this.dataGVsubtitlelines.Size = new System.Drawing.Size(502, 561);
            this.dataGVsubtitlelines.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Column5";
            this.Column5.Name = "Column5";
            // 
            // dataGVsubtitles
            // 
            this.dataGVsubtitles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGVsubtitles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dataGVsubtitles.Location = new System.Drawing.Point(539, 47);
            this.dataGVsubtitles.Name = "dataGVsubtitles";
            this.dataGVsubtitles.Size = new System.Drawing.Size(267, 183);
            this.dataGVsubtitles.TabIndex = 8;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // btnRead_subtitlelines
            // 
            this.btnRead_subtitlelines.Location = new System.Drawing.Point(13, 13);
            this.btnRead_subtitlelines.Name = "btnRead_subtitlelines";
            this.btnRead_subtitlelines.Size = new System.Drawing.Size(124, 23);
            this.btnRead_subtitlelines.TabIndex = 9;
            this.btnRead_subtitlelines.Text = "Read subtitlelines";
            this.btnRead_subtitlelines.UseVisualStyleBackColor = true;
            this.btnRead_subtitlelines.Click += new System.EventHandler(this.btnRead_subtitlelines_Click);
            // 
            // btnRead_subtitles
            // 
            this.btnRead_subtitles.Location = new System.Drawing.Point(539, 9);
            this.btnRead_subtitles.Name = "btnRead_subtitles";
            this.btnRead_subtitles.Size = new System.Drawing.Size(159, 23);
            this.btnRead_subtitles.TabIndex = 10;
            this.btnRead_subtitles.Text = "Read subtitles";
            this.btnRead_subtitles.UseVisualStyleBackColor = true;
            this.btnRead_subtitles.Click += new System.EventHandler(this.btnRead_subtitles_Click);
            // 
            // dataGVvideofiles
            // 
            this.dataGVvideofiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGVvideofiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dataGVvideofiles.Location = new System.Drawing.Point(539, 349);
            this.dataGVvideofiles.Name = "dataGVvideofiles";
            this.dataGVvideofiles.Size = new System.Drawing.Size(360, 259);
            this.dataGVvideofiles.TabIndex = 11;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Column3";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // btnRead_videofiles
            // 
            this.btnRead_videofiles.Location = new System.Drawing.Point(539, 262);
            this.btnRead_videofiles.Name = "btnRead_videofiles";
            this.btnRead_videofiles.Size = new System.Drawing.Size(101, 23);
            this.btnRead_videofiles.TabIndex = 12;
            this.btnRead_videofiles.Text = "Read videofiles";
            this.btnRead_videofiles.UseVisualStyleBackColor = true;
            this.btnRead_videofiles.Click += new System.EventHandler(this.btnRead_videofiles_Click);
            // 
            // btnInsert_videofiles
            // 
            this.btnInsert_videofiles.Location = new System.Drawing.Point(539, 291);
            this.btnInsert_videofiles.Name = "btnInsert_videofiles";
            this.btnInsert_videofiles.Size = new System.Drawing.Size(101, 23);
            this.btnInsert_videofiles.TabIndex = 13;
            this.btnInsert_videofiles.Text = "Insert videofiles";
            this.btnInsert_videofiles.UseVisualStyleBackColor = true;
            this.btnInsert_videofiles.Click += new System.EventHandler(this.btnInsert_videofiles_Click);
            // 
            // btnUpdate_videofiles
            // 
            this.btnUpdate_videofiles.Location = new System.Drawing.Point(539, 320);
            this.btnUpdate_videofiles.Name = "btnUpdate_videofiles";
            this.btnUpdate_videofiles.Size = new System.Drawing.Size(101, 23);
            this.btnUpdate_videofiles.TabIndex = 14;
            this.btnUpdate_videofiles.Text = "Update videofiles";
            this.btnUpdate_videofiles.UseVisualStyleBackColor = true;
            this.btnUpdate_videofiles.Click += new System.EventHandler(this.btnInsert_videofiles_Click);
            // 
            // btnDelete_videofiles
            // 
            this.btnDelete_videofiles.Location = new System.Drawing.Point(689, 262);
            this.btnDelete_videofiles.Name = "btnDelete_videofiles";
            this.btnDelete_videofiles.Size = new System.Drawing.Size(101, 23);
            this.btnDelete_videofiles.TabIndex = 15;
            this.btnDelete_videofiles.Text = "Delete videofiles";
            this.btnDelete_videofiles.UseVisualStyleBackColor = true;
            this.btnDelete_videofiles.Click += new System.EventHandler(this.btnInsert_videofiles_Click);
            // 
            // btnClear_videofiles
            // 
            this.btnClear_videofiles.Location = new System.Drawing.Point(689, 291);
            this.btnClear_videofiles.Name = "btnClear_videofiles";
            this.btnClear_videofiles.Size = new System.Drawing.Size(101, 23);
            this.btnClear_videofiles.TabIndex = 15;
            this.btnClear_videofiles.Text = "Clear videofiles";
            this.btnClear_videofiles.UseVisualStyleBackColor = true;
            this.btnClear_videofiles.Click += new System.EventHandler(this.btnInsert_videofiles_Click);
            // 
            // btnRollback_videofiles
            // 
            this.btnRollback_videofiles.Location = new System.Drawing.Point(689, 320);
            this.btnRollback_videofiles.Name = "btnRollback_videofiles";
            this.btnRollback_videofiles.Size = new System.Drawing.Size(119, 23);
            this.btnRollback_videofiles.TabIndex = 15;
            this.btnRollback_videofiles.Text = "Rollback videofiles";
            this.btnRollback_videofiles.UseVisualStyleBackColor = true;
            this.btnRollback_videofiles.Click += new System.EventHandler(this.btnRollback_videofiles_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 620);
            this.Controls.Add(this.btnRollback_videofiles);
            this.Controls.Add(this.btnClear_videofiles);
            this.Controls.Add(this.btnDelete_videofiles);
            this.Controls.Add(this.btnUpdate_videofiles);
            this.Controls.Add(this.btnInsert_videofiles);
            this.Controls.Add(this.btnRead_videofiles);
            this.Controls.Add(this.dataGVvideofiles);
            this.Controls.Add(this.btnRead_subtitles);
            this.Controls.Add(this.btnRead_subtitlelines);
            this.Controls.Add(this.dataGVsubtitles);
            this.Controls.Add(this.dataGVsubtitlelines);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGVsubtitlelines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGVsubtitles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGVvideofiles)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGVsubtitlelines;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridView dataGVsubtitles;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button btnRead_subtitlelines;
        private System.Windows.Forms.Button btnRead_subtitles;
        private System.Windows.Forms.DataGridView dataGVvideofiles;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.Button btnRead_videofiles;
        private System.Windows.Forms.Button btnInsert_videofiles;
        private System.Windows.Forms.Button btnUpdate_videofiles;
        private System.Windows.Forms.Button btnDelete_videofiles;
        private System.Windows.Forms.Button btnClear_videofiles;
        private System.Windows.Forms.Button btnRollback_videofiles;
    }
}

