﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Issue58
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnRead_subtitlelines_Click(object sender, EventArgs e)
        {
            dataGVsubtitlelines.DataSource = null;
            dataGVsubtitlelines.Rows.Clear();
            dataGVsubtitlelines.Refresh();
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("data source=DebugJetmoji.db"))
            {
                using (System.Data.SQLite.SQLiteCommand com = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();                             // Open the connection to the database

                    com.CommandText = "Select * FROM subtitlelines";      // Select all rows from our database table
                    Console.WriteLine("Display subtitlelines");

                    dataGVsubtitlelines.Columns[0].HeaderText = "sid";
                    dataGVsubtitlelines.Columns[1].HeaderText = "indexid";
                    dataGVsubtitlelines.Columns[2].HeaderText = "starttime";
                    dataGVsubtitlelines.Columns[3].HeaderText = "endtime";
                    dataGVsubtitlelines.Columns[4].HeaderText = "content";
                    using (System.Data.SQLite.SQLiteDataReader reader = com.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            dataGVsubtitlelines.Rows.Add(reader["sid"], reader["indexid"], reader["starttime"], reader["endtime"], reader["content"]);
                        }
                    }
                    con.Close();        // Close the connection to the database
                }
            }
        }

        private void btnRead_subtitles_Click(object sender, EventArgs e)
        {
            dataGVsubtitles.DataSource = null;
            dataGVsubtitles.Rows.Clear();
            dataGVsubtitles.Refresh();

            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("data source=DebugJetmoji.db"))
            {
                using (System.Data.SQLite.SQLiteCommand com = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();                             // Open the connection to the database

                    com.CommandText = "Select * FROM subtitles";      // Select all rows from our database table
                    Console.WriteLine("Display subtitles");
                    dataGVsubtitles.Columns[0].HeaderText = "sID";
                    dataGVsubtitles.Columns[1].HeaderText = "sName";
                    using (System.Data.SQLite.SQLiteDataReader reader = com.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            dataGVsubtitles.Rows.Add(reader["sID"], reader["sName"]);
                        }
                    }
                    con.Close();        // Close the connection to the database
                }
            }
        }

        private void btnRead_videofiles_Click(object sender, EventArgs e)
        {
            dataGVvideofiles.DataSource = null;
            dataGVvideofiles.Rows.Clear();
            dataGVvideofiles.Refresh();

            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("data source=DebugJetmoji.db"))
            {
                using (System.Data.SQLite.SQLiteCommand com = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();                             // Open the connection to the database

                    com.CommandText = "Select * FROM videofiles";      // Select all rows from our database table
                    Console.WriteLine("Display videofiles");

                    dataGVvideofiles.Columns[0].HeaderText = "fID";
                    dataGVvideofiles.Columns[1].HeaderText = "sID";
                    dataGVvideofiles.Columns[2].HeaderText = "fName";
                    using (System.Data.SQLite.SQLiteDataReader reader = com.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            dataGVvideofiles.Rows.Add(reader["fID"], reader["sID"], reader["fName"]);
                        }
                    }
                    con.Close();        // Close the connection to the database
                }
            }
        }

        public DataTable GetDataTable(string tablename)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("data source=DebugJetmoji.db"))
            {
                DataTable DT = new DataTable();
                con.Open();
                SQLiteCommand cmd = con.CreateCommand();
                cmd.CommandText = string.Format("SELECT * FROM {0}", tablename);
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(cmd);
                adapter.Fill(DT);
                con.Close();
                DT.TableName = tablename;
                return DT;
            }
        }

        public void SaveDataTable(DataTable DT)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("data source=DebugJetmoji.db"))
            {
                //try
                {
                    con.Open();
                    SQLiteCommand cmd = con.CreateCommand();
                    cmd.CommandText = string.Format("SELECT * FROM {0}", DT.TableName);
                    SQLiteDataAdapter adapter = new SQLiteDataAdapter(cmd);
                    SQLiteCommandBuilder builder = new SQLiteCommandBuilder(adapter);
                    adapter.Update(DT);
                    con.Close();
                }
                /*catch (Exception Ex)
                {
                    System.Windows.MessageBox.Show(Ex.Message);
                }*/
            }
        }

        private void btnInsert_videofiles_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string strBtnCaption = btn.Text;

            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("data source=DebugJetmoji.db"))
            {
                con.Open();
                DataTable DT = GetDataTable("videofiles");

                SQLiteCommand mycommand = new SQLiteCommand(con);
                String strSqlCommand;
                    
                if (strBtnCaption.Contains("Update"))
                    strSqlCommand = "update videofiles set fName = 'update' where sID = 1;";
                else if (strBtnCaption.Contains("Delete"))
                    strSqlCommand = "delete from videofiles where sID = 1;";
                else if (strBtnCaption.Contains("Clear"))
                    strSqlCommand = "delete from videofiles;";
                else
                    strSqlCommand = "INSERT INTO videofiles(fID, sID, fName) VALUES (0,0,'test');";

                mycommand.CommandText = strSqlCommand;
                int rowsUpdated = mycommand.ExecuteNonQuery();

                SaveDataTable(DT);

                con.Close();        // Close the connection to the database
            }
            btnRead_videofiles_Click(sender, e);
        }

        private void btnRollback_videofiles_Click(object sender, EventArgs e)
        {
            System.IO.File.Copy(Application.StartupPath + "\\Source.db", Application.StartupPath + "\\DebugJetmoji.db", true);
            btnRead_videofiles_Click(sender, e);
        }
    }
}
